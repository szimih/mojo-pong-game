`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:41:51 12/18/2014 
// Design Name: 
// Module Name:    clk_100 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clk_100k(
    input clk, rst,
    output out
    );
	 
reg [16:0] Q;	 
	 
always @(posedge clk)
	if(out || rst)
		Q = 0;
	else
		Q = Q + 1;

assign out = (Q == 999999);

endmodule
