`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:07:17 12/18/2014 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(	
    input clk, 
    input rst_n,   
    output[7:0]led,
	 
    /*input cclk,
    output spi_miso,
    input spi_ss,
    input spi_mosi,
    input spi_sck,   
    output [3:0] spi_channel,    
    input avr_tx,
    output avr_rx, 
    input avr_rx_busy,*/
	 input [3:0]btn,
	 output h_sync , v_sync, vga_r, vga_g, vga_b
	 
    );
	 
wire rst = ~rst_n;

wire clk100k;
clk_100k clk_100(.clk(clk), .rst(rst), .out(clk100k));

wire video_on;
wire [10:0] x;
wire [9:0] y;
wire [2:0] rgb_out;
wire [2:0] rgb;
	 
vga_sync_my vga(clk, rst, h_sync , v_sync, video_on, x, y);

wire [3:0] p1_s, p2_s;

my_ball ball(.clk(clk), .reset(rst), .x(x), .y(y), .rgb(rgb_out), .ball_rgb(3'b111), 
.p1_btn(btn[1:0]), .p1_rgb(3'b100), .p2_btn(btn[3:2]), .p2_rgb(3'b001), .p1_score(p1_s), .p2_score(p2_s));

wire [2:0] rgb_font;

text_w text(.clk(clk), .x(x), .y(y), .rgb(rgb_font), .p1s(p1_s), .p2s(p2_s));

assign rgb = (video_on) ? (rgb_out | rgb_font) : 3'b000;

assign vga_r = (rgb[0]) ? 1'b1 : 1'b0;
assign vga_g = (rgb[1]) ? 1'b1 : 1'b0;
assign vga_b = (rgb[2]) ? 1'b1 : 1'b0;

assign led = {p1_s, p2_s};

endmodule
