`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:42:20 12/18/2014 
// Design Name: 
// Module Name:    my_ball 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module my_ball(
input clk,reset,
input wire [10:0] x,y,
output reg [2:0] rgb,
input [2:0] ball_rgb,
input [2:0] p1_rgb,
input [1:0] p1_btn,
input [2:0] p2_rgb,
input [1:0] p2_btn,
output [3:0] p1_score,p2_score
    );
	 
wire tick;
assign tick = ((x == 0) && (y == 601));

localparam WIDTH = 800;
localparam HEIGHT = 600;
localparam MIN_HEIGHT = 35;

localparam PLAYER1_X_L = 794;
localparam PLAYER1_X_R = 800;

localparam PLAYER2_X_L = 0;
localparam PLAYER2_X_R = 6;

localparam BAR_SIZE = 72;
localparam BAR_VM = 4;

wire [9:0] PLAYER1_Y_T, PLAYER1_Y_B, PLAYER2_Y_T, PLAYER2_Y_B;
reg [9:0] PLAYER1_Y_REG, PLAYER1_Y_NEXT, PLAYER2_Y_REG, PLAYER2_Y_NEXT;

reg  [6:0]  p1_s,  p2_s;

localparam BALL_SIZE = 20;

wire ball_on;

wire [10:0] ball_x_l, ball_x_r, ball_y_t, ball_y_b, ball_x_next, ball_y_next;
reg [10:0] ball_x_reg, ball_y_reg;

assign ball_x_l =  ball_x_reg;
assign ball_x_r =  ball_x_reg + BALL_SIZE - 1;
assign ball_y_t =  ball_y_reg;
assign ball_y_b =  ball_y_reg + BALL_SIZE - 1;


reg [10:0] x_d_r, x_d_n, y_d_r, y_d_n; 

assign ball_on = (ball_x_l < x ) && (ball_y_t < y ) && (x < ball_x_r) && (y < ball_y_b);

reg PLAYER2_W;
reg PLAYER1_W;

always@(posedge clk, posedge reset)
	if(reset)
		begin
			PLAYER1_Y_REG 	<=	MIN_HEIGHT; 
			PLAYER2_Y_REG 	<=	MIN_HEIGHT; 
			ball_x_reg 		<= 400;
			ball_y_reg 		<= 300;
			x_d_r 			<=	1;
			y_d_r 			<=	1;
			p1_s 				<= 4'b0000;
			p2_s 				<= 4'b0000;
		end
	else
		begin
			PLAYER1_Y_REG	<= PLAYER1_Y_NEXT;
			PLAYER2_Y_REG	<= PLAYER2_Y_NEXT;
			
			if(PLAYER1_W)
				 p2_s <=  p2_s + 1;
			if(PLAYER2_W)
				 p1_s <=  p1_s + 1;					 
			
			if(p1_s == 10)
				p1_s <= 4'b0000;
		   if(p2_s == 10)
				p2_s <= 4'b0000;
			
			if(PLAYER2_W || PLAYER1_W)
				begin	
					ball_x_reg 		<= 400;
					ball_y_reg 		<= 300;
				end
			else
				begin
					ball_x_reg 		<= ball_x_next;
					ball_y_reg 		<= ball_y_next;
				end
			x_d_r 			<=	x_d_n;
			y_d_r 			<=	y_d_n;
		end
			
			
assign PLAYER1_Y_T = PLAYER1_Y_REG;
assign PLAYER1_Y_B = PLAYER1_Y_REG + BAR_SIZE - 1;		
assign PLAYER2_Y_T = PLAYER2_Y_REG;
assign PLAYER2_Y_B = PLAYER2_Y_REG + BAR_SIZE - 1;			
			
wire PLAYER1_ON;
assign PLAYER1_ON	= (PLAYER1_X_L < x) && (x < PLAYER1_X_R) && (PLAYER1_Y_T < y) && (y < PLAYER1_Y_B);

wire PLAYER2_ON;
assign PLAYER2_ON	= (PLAYER2_X_L < x) && (x < PLAYER2_X_R) && (PLAYER2_Y_T < y) && (y < PLAYER2_Y_B);
			
wire PALYA_ON;
assign PALYA_ON = (0 < x) && (x < WIDTH) && ( MIN_HEIGHT < y) && (y < HEIGHT);

wire KOZEPVONAL_ON;
assign KOZEPVONAL_ON = (((WIDTH/2)-2) < x) && (x < ((WIDTH/2)+2)) && ( MIN_HEIGHT < y) && (y < HEIGHT);
			
assign ball_x_next = (tick) ? ball_x_reg + x_d_r : ball_x_reg;	
assign ball_y_next = (tick) ? ball_y_reg + y_d_r : ball_y_reg;

always@*
	begin
		PLAYER1_Y_NEXT = PLAYER1_Y_REG;
		PLAYER2_Y_NEXT = PLAYER2_Y_REG;
		if(tick)
			begin
				if(p1_btn[0] & (PLAYER1_Y_B < (HEIGHT - 1 - BAR_VM)))
					PLAYER1_Y_NEXT = PLAYER1_Y_REG + BAR_VM;
				else if(p1_btn[1] & (PLAYER1_Y_T > (BAR_VM + MIN_HEIGHT)))
					PLAYER1_Y_NEXT = PLAYER1_Y_REG - BAR_VM;
					
				if(p2_btn[0] & (PLAYER2_Y_B < (HEIGHT - 1 - BAR_VM)))
					PLAYER2_Y_NEXT = PLAYER2_Y_REG + BAR_VM;
				else if(p2_btn[1] & (PLAYER2_Y_T > (BAR_VM + MIN_HEIGHT)))
					PLAYER2_Y_NEXT = PLAYER2_Y_REG - BAR_VM;
			end
	end
	
always@*
	begin
		x_d_n = x_d_r;
		y_d_n = y_d_r;
		PLAYER1_W = 1'b0;
		PLAYER2_W = 1'b0;
		if( ball_y_t < MIN_HEIGHT )
			 y_d_n = 2;
		else if( ball_y_b > ( HEIGHT - 1 ) )
				y_d_n = -2;
				else if((PLAYER2_X_L < ball_x_l) && (ball_x_l < PLAYER2_X_R) && (PLAYER2_Y_T < ball_y_b) && (ball_y_t < PLAYER2_Y_B))
					x_d_n = 2;
					else if( ball_x_l < 3 )
							PLAYER1_W = 1'b1;
							else if((PLAYER1_X_L < ball_x_r) && (ball_x_r < PLAYER1_X_R) && (PLAYER1_Y_T < ball_y_b) && (ball_y_t < PLAYER1_Y_B))
									x_d_n = -2;
									else if( ball_x_r > ( WIDTH - 3) )
											PLAYER2_W = 1'b1;
	end
							
always @*
	if(ball_on)
		rgb = ball_rgb;
	else if(PLAYER1_ON)
		rgb = p1_rgb;
		else if(PLAYER2_ON)
			rgb = p2_rgb;
				else if(KOZEPVONAL_ON)
				rgb = 3'b111;
					else if(PALYA_ON)
						rgb = 3'b010;
							else
								rgb = 3'b000;
				
assign p1_score = p1_s;
assign p2_score = p2_s;

endmodule
 