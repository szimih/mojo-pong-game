`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:56:54 12/18/2014 
// Design Name: 
// Module Name:    text_w 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module text_w(
    input clk,
    input [10:0] x,
    input [9:0] y,
    output reg [2:0] rgb,
	 input [3:0] p1s, p2s
    );

wire [7:0] font_word;
wire [10:0] rom_addr;
wire score_on, font_en;
wire [3:0] row_addr_s;
wire [2:0] bit_addr_s;
reg [6:0] char_addr_s, char_addr;
reg [3:0] row_addr;
reg [2:0] bit_addr;

font_rom f_r(.clka(clk), .addra(rom_addr), .douta(font_word));

assign score_on = (y[9:5] == 0) && (x[9:4] < 16);
assign row_addr_s = y[4:1];
assign bit_addr_s = x[3:1];

always @*
	case(x[7:4])
		4'h0 : char_addr_s = 7'h50; // P
		4'h1 : char_addr_s = 7'h6f; // o
		4'h2 : char_addr_s = 7'h6e; // n
		4'h3 : char_addr_s = 7'h74; // t
		4'h4 : char_addr_s = 7'h6f; // o
		4'h5 : char_addr_s = 7'h6b; // k
		4'h6 : char_addr_s = 7'h00; // 
		4'h7 : char_addr_s = 7'h50; // P
		4'h8 : char_addr_s = 7'h31; // 1
		4'h9 : char_addr_s = 7'h3a; // :
		4'ha : char_addr_s = {3'b011, p1s}; // ertek
		4'hb : char_addr_s = 7'h00; // 
		4'hc : char_addr_s = 7'h50; // P
		4'hd : char_addr_s = 7'h32; // 2
		4'he : char_addr_s = 7'h3a; // :
		4'hf : char_addr_s = {3'b011, p2s}; // ertek
	endcase

always @ *
	begin
		rgb = 3'b000;
		if(score_on)
			begin
				char_addr 	= char_addr_s;
				row_addr 	= row_addr_s;
				bit_addr		= bit_addr_s;
				if(font_en)
					rgb = 3'b111;
			end
	end
		

assign rom_addr = {char_addr, row_addr};
assign font_en = font_word[~bit_addr];

endmodule

