`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:47:24 07/09/2014 
// Design Name: 
// Module Name:    vga_sync_my 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module vga_sync_my(
	input clk, reset,
	output  wire h_sync , v_sync, video_on,
	output reg [10:0] pos_x,
	output reg [9:0] pos_y
    );

localparam  HD = 800;
localparam  HF = 64;
localparam  HB  = 56;
localparam  HR = 120;

localparam  VD = 600;
localparam  VF  = 37; 
localparam  VB  = 23; 
localparam  VR = 6;

assign h_sync = ( pos_x >= (HD+HB) && pos_x <= (HD+HB+HR-1) );
assign v_sync = ( pos_y >= (VD+VB) && pos_y <= (VD+VB+VR-1) ); 

assign video_on =  ( pos_x < HD ) && ( pos_y < VD ); 

always @ (posedge clk, posedge reset)
	if(reset)
		begin
			pos_x <= 0;
			pos_y <= 0;
		end
	else if( pos_x == (HD+HF+HB+HR-1) )
				begin
					pos_x <= 0;
					if ( pos_y == (VD+VF+VB+VR-1) )
						pos_y <= 0;
					else
						pos_y <= pos_y + 1;
				end
			else
				pos_x <= pos_x + 1;
			
endmodule